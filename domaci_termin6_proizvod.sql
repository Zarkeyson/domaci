DOMACI
1.
select proizvod.naziv, drzava.naziv
from proizvod
join proizvodjac on proizvodjac.proizvodjacid = proizvod.proizvodjac_id
join grad on grad.id = proizvodjac.grad_id
join drzava on drzava.kod = grad.drzava_kod
group by proizvod.naziv ASC

2.
select proizvodjac.naziv as Proizvodjac,
(select count(*) from proizvod where proizvodjac.proizvodjacid=proizvod.proizvodjac_id) as "Broj proizvoda"
from proizvodjac

3.
select prodavnica.naziv
from prodavnica
join grad on grad.id = prodavnica.grad_id
where grad.naziv = "Dusseldorf"

4.
select *
from prodavnica_proizvod
join prodavnica on prodavnica.prodavnicaID = prodavnica_proizvod.prodavnica_id
join proizvod on proizvod.proizvodID = prodavnica_proizvod.proizvod_id
where prodavnica.naziv = "Audi-Salon"

5.
select proizvod.naziv from proizvod
where proizvod.kategorija_id = 1;

6.
select kategorija.naziv, count(proizvod.proizvodID) from kategorija
left join proizvod on kategorija.idkategorija = proizvod.kategorija_id
group by kategorija.idkategorija





CREATE DATABASE  IF NOT EXISTS `domaci_termin6` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `domaci_termin6`;
-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: domaci_termin6
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `proizvod`
--

DROP TABLE IF EXISTS `proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvod` (
  `proizvodID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `barkod` varchar(13) NOT NULL,
  `proizvodjac_id` int(11) DEFAULT NULL,
  `kategorija_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`proizvodID`),
  KEY `fk_proizvod_1_idx` (`proizvodjac_id`),
  KEY `fk_proizvod_2_idx` (`kategorija_id`),
  CONSTRAINT `fk_proizvod_1` FOREIGN KEY (`proizvodjac_id`) REFERENCES `proizvodjac` (`proizvodjacid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proizvod_2` FOREIGN KEY (`kategorija_id`) REFERENCES `kategorija` (`idkategorija`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvod`
--

LOCK TABLES `proizvod` WRITE;
/*!40000 ALTER TABLE `proizvod` DISABLE KEYS */;
INSERT INTO `proizvod` VALUES (1,'Audi A5 SLine','100',8,1),(2,'Audi A7 SLine','101',8,1),(3,'Audi A4 Karavan','102',8,2),(4,'Mercedes S400','103',9,3),(5,'Fiat YUGOSLAVIJA','104',14,1),(6,'Porsche 911','105',8,1),(7,'Porsche Cayman S','106',13,2);
/*!40000 ALTER TABLE `proizvod` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-04-29 13:47:44
